import _init
import os
import argparse
import pickle
import platform
import progressbar
import numpy as np

from PIL.ImageDraw import Draw

from data.datasets import load_dataset
from encoding_config import config
from tools import torch_tools
from data import data_feeder

from encoding_dnn.data import imageset_blob
from encoding_dnn.data import patch_blobs
from encoding_dnn.networks import networks

import torch
import torchvision.transforms as transforms

from dnn import trainer

def parse_commandline():
    parser = argparse.ArgumentParser(description="Test the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    parser.add_argument('-b','--batch_size',help='Batch size', required=True)
    parser.add_argument('-t','--tag',help='Model tag', required=True)
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return vars(parser.parse_args())

class my_trainer(trainer):
    def test(self):
        self._model['net'].eval()
        out_list = []

        testset_len = len(self._testset)
        for idx in range(len(self._testset)):
            inputs, targets = self._testset.next()

            category = int(targets['gtcategory'].numpy())
            inputs = self._set_device( inputs )
            targets = self._set_device( targets )

            output = self._model['net'](inputs)

            code = output.detach().cpu().numpy().reshape(-1,2048)
            out_list.append({'code':code, 'category':category})
            if idx % 1000 == 0:
                print('testing {} / {}'.format(idx, testset_len))
        return out_list

    def test_one(self):
        self._model['net'].eval()
        inputs, targets = self._testset.next()
        output = self._model['net'](inputs)

        return {'code': output, 'label':targets}

    def _train( self ):
        self._model['net'].train()

        widgets = [ progressbar.Percentage(), ' ', progressbar.ETA(), ' ',
                    '(',progressbar.DynamicMessage('loss'),')' ]
        bar = progressbar.ProgressBar(widgets=widgets,max_value=len(self._trainset)).start()

        loss_values = []

        for idx in range( len(self._trainset) ):
            inputs, targets = self._trainset.next()

            inputs = self._set_device( inputs )
            targets = self._set_device( targets )

            outputs = self._model['net']( inputs)
            loss = self._model['loss']( outputs, targets )

            loss_values.append( loss.cpu().detach().numpy() )

            # Computing gradient and do SGD step
            self._optimizer.zero_grad()
            loss.backward()
            self._optimizer.step()

            bar.update(idx+1,loss=loss.item())
        bar.finish()

        print('Average loss : ', np.mean(loss_values))

def get_dataset():
  #args = parse_commandline()
    params = {}
    params['config'] = 'patch_dataset'
    params['batch_size'] = 1
    params['gpu'] = '0'
    params['tag'] = '20181031'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    test_dataset = load_dataset( cfg, ['deepfashion','capb','test'] )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], test_dataset.hash, model_hash='vgg16' )

    #train_imageset = imageset_hdf5(cfg, train_dataset, is_training=True, randomize=True)
    blob_gen = patch_blobs(cfg.dnn)
    test_imageset = imageset_blob(cfg, test_dataset, blob_gen, is_training=False)

    test_feeder = data_feeder(test_imageset)

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    model = {}
    #model['net'] = networks['gtbox'].Net( cfg ).to( device )
    #model['loss'] = networks['gtbox'].Loss( cfg ).to( device )
    model['net'] = networks.category_embedding.testnet(cfg).to(device)
    #model['loss'] = networks.category_embedding.loss(cfg).to(device)
    print('Load model from {}'.format(cfg.path.MODEL))
    state_dict = torch.load(cfg.path.MODEL, map_location=device)['state_dict']
    miss = model['net'].load_state_dict(state_dict, strict=False)
    model['net'].to(device)

    t = my_trainer( cfg, model, device, trainset=None, testset=test_feeder )
    #loss = t.trainstep()
    #print('loss is {}'.format(loss))
    out = t.test()

    test_feeder.exit()

    return out, miss, state_dict

def get_input(train_feeder):
    data = train_feeder.next()
    for key in data.keys():
        for a_key in data[key].keys():
            print('Key: {}, shape: {}'.format(a_key, data[key][a_key].shape))
    #return data

if __name__=="__main__" :
    params = parse_commandline()
    print('batch size is {}'.format(params['batch_size']))
    params['config'] = 'patch_dataset'
    params['gpu'] = '0'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    test_dataset = load_dataset( cfg, ['deepfashion','capb','test'] )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], test_dataset.hash, model_hash='vgg16' )

    #train_imageset = imageset_hdf5(cfg, train_dataset, is_training=True, randomize=True)
    blob_gen = patch_blobs(cfg.dnn)
    test_imageset = imageset_blob(cfg, test_dataset, blob_gen, is_training=False)

    test_feeder = data_feeder(test_imageset)

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    model = {}
    model['net'] = networks.category_embedding.testnet(cfg).to(device)
    #model['loss'] = networks.category_embedding.loss(cfg).to(device)
    print('Load model from {}'.format(cfg.path.MODEL))
    state_dict = torch.load(cfg.path.MODEL, map_location=device)['state_dict']
    model['net'].load_state_dict(state_dict, strict=False)
    model['net'].to(device)

    t = my_trainer( cfg, model, device, trainset=None, testset=test_feeder )

    start = time.time()
    print('start testing')
    out = t.test()
    print('Used {:.2f} seconds to test'.format(time.time()-start))
    with open('result.pkl', 'wb') as f:
        pickle.dump(out, f)

    test_feeder.exit()
