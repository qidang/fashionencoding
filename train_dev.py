import _init
import os
import argparse
import pickle
import platform

if __name__=="__main__" :
    parser = argparse.ArgumentParser(description='Detector Training')
    parser.add_argument('-c','--config',help='Configuration Name',required=True)
    parser.add_argument('-b','--batchsize',help='Batch size', default=16 )
    parser.add_argument('-t','--tag',help='Data Tag', default=None)
    parser.add_argument('--datasetpath', help='Path to the image dataset', required=True )
    parser.add_argument('--projectpath', help='Path to save the data', required=True )
    parser.add_argument('-g','--gpu',help='GPU Index',default='0')
    parser.set_defaults(finetune=False)
    args = parser.parse_args()
    #params = _init.parse_commandline()

    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu


from PIL.ImageDraw import Draw

from data.datasets import load_dataset
from encoding_config import config
from tools import torch_tools
from data import data_feeder

from encoding_dnn.data import imageset_hdf5
from encoding_dnn.networks import networks

import torch
import torchvision.transforms as transforms

from dnn import trainer

def get_dataset():
  #args = parse_commandline()
    params = {}
    params['config'] = 'patch_dataset'
    params['batch_size'] = 32
    params['gpu'] = '0'
    params['tag'] = '20181031'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    train_dataset = load_dataset( cfg, ['deepfashion','capb','train'] )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], train_dataset.hash, model_hash='vgg16' )

    train_imageset = imageset_hdf5(cfg, train_dataset, is_training=True, randomize=True)

    train_feeder = data_feeder(train_imageset)

    return train_feeder

def get_input(train_feeder):
    data = train_feeder.next()
    for key in data.keys():
        print(key)
        for a_key in data[key].keys():
            print('Key: {}-{}, shape: {}'.format(key, a_key, data[key][a_key].shape))
    return data

def get_model():
    params = {}
    params['config'] = 'patch_dataset'
    params['batch_size'] = 32
    params['gpu'] = '0'
    params['tag'] = '20181031'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    device = torch_tools.get_device( params['gpu'] )

    model = {}
    model['net'] = networks.category_embedding.net(cfg).to(device)
    model['loss'] = networks.category_embedding.loss(cfg).to(device)
    print("Model's state_dict:")
    for param_tensor in model['net'].state_dict():
        print(param_tensor, "\t", model['net'].state_dict()[param_tensor].size())

    return model


if __name__=="__main__" :
    params = vars( args )

    params['nclasses'] = 3
    params['ncategories'] = 46

    params['dset'] = ['deepfashion','capb','train']
    params['prefix'] = 'DeepfashionCap'

    cfg = config( params['config'], 'train', params['datasetpath'], params['projectpath'] )
    cfg.update( params )

    train_dataset = load_dataset( cfg, ['deepfashionp','capb','train'] )
    #test_dataset = load_dataset( cfg, ['deepfashionp','capb','test'], settings=[] )

    cfg.build_path( params['tag'], train_dataset.hash )

    preprocess = []
    preprocess.append(transforms.ToTensor())
    preprocess.append(transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]))

    blobs_gen = blobs[cfg.dnn.NETWORK.INTERFACE]( cfg, preprocess=preprocess )

    train_batches = batch_generator( cfg, train_dataset, blobs_gen, randomize=True, is_training=True )
    train_feeder = data_feeder( train_batches )

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    model = {}
    model['net'] = networks['gtbox'].Net( cfg ).to( device )
    model['loss'] = networks['gtbox'].Loss( cfg ).to( device )

    t = trainer( cfg, model, device, train_feeder )
    t.train()

    train_feeder.exit()

    torch.save({'state_dict':model['net'].state_dict()}, cfg.path.MODEL)

    print('Model is saved to :', cfg.path.MODEL)
