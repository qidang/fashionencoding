from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'gtbox_kp_vgg16'

    #cfg.DNN.PROPOSAL.TRAIN.SELECTION_BATCH_SIZE = {'s1':128,'s2':128}
    #cfg.DNN.FRCNN.TRAIN.SELECTION_BATCH_SIZE = {'s1':256,'s2':256,'s2_hinge':2000}

    cfg.DNN['train'].NITER = 8
    cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.01, 'momentum':0.9, 'decay_step':4,'decay_rate':0.1}

    cfg.NETWORK.INTERFACE = 'gtbox_kp'
    cfg.NETWORK.FEAT_NAME = 'vgg16'

    return cfg
