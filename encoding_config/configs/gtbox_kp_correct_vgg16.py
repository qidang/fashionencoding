from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'gtbox_kp_correct_vgg16'

    #cfg.DNN['train'].MAX_SIZE = 224
    #cfg.DNN['test'].MAX_SIZE = 224
    cfg.DNN['train'].NITER = 20
    cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.01, 'momentum':0.9, 'decay_step':7,'decay_rate':0.1}

    #cfg.DNN.PROPOSAL.TRAIN.SELECTION_BATCH_SIZE = {'s1':128,'s2':128}
    #cfg.DNN.FRCNN.TRAIN.SELECTION_BATCH_SIZE = {'s1':256,'s2':256,'s2_hinge':2000}

    #for blob setting
    cfg.NETWORK.INTERFACE = 'gtbox_kp_correct'
    cfg.NETWORK.FEAT_NAME = 'vgg16'
    cfg.NETWORK.FEATURE_LEVEL = -3 # -1 means the last one (stride = 16)

    return cfg
