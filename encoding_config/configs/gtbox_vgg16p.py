from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'gtbox_vgg16p'

    #cfg.DNN.PROPOSAL.TRAIN.SELECTION_BATCH_SIZE = {'s1':128,'s2':128}
    #cfg.DNN.FRCNN.TRAIN.SELECTION_BATCH_SIZE = {'s1':256,'s2':256,'s2_hinge':2000}

    cfg.NETWORK.INTERFACE = 'gtbox'
    cfg.NETWORK.FEAT_NAME = 'vgg16'
    cfg.NETWORK.PRETRAINED = 'vgg_16_2016_08_28'

    cfg.DNN['train'].NITER = 500000
    cfg.DNN['train'].OPTIMIZER = {'type':'GD','lr':0.001,'decay_step':250000,'decay_rate':0.1}


    return cfg
