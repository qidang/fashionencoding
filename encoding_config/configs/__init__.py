from . import base as baseline
from . import conf_proposal
from . import gtbox_vgg16
from . import gtbox_kp_vgg16
from . import gtbox_vgg16p
from . import gtbox_ms_vgg16
from . import gtbox_vgg_small
from . import patch_vgg16
from . import patch_vgg16p
from . import patch_vgg_small
from . import gtbox_kp_correct_vgg16
from . import patch_dataset


configurations = {}

configurations['baseline'] = baseline
configurations['conf_proposal'] = conf_proposal
configurations['gtbox_vgg16'] = gtbox_vgg16
configurations['gtbox_kp_vgg16'] = gtbox_kp_vgg16
configurations['gtbox_ms_vgg16'] = gtbox_ms_vgg16
configurations['gtbox_vgg16p'] = gtbox_vgg16p
configurations['gtbox_vgg_small'] = gtbox_vgg_small
configurations['patch_vgg16'] = patch_vgg16
configurations['patch_vgg16p'] = patch_vgg16p
configurations['patch_vgg_small'] = patch_vgg_small
configurations['gtbox_kp_correct_vgg16'] = gtbox_kp_correct_vgg16
configurations['patch_dataset'] = patch_dataset
