from easydict import EasyDict as edict
import numpy as np
from . import base as baseline

def get_cfg():
    cfg = baseline.get_cfg()

    # Modifications to baseline config
    cfg.NAME = 'patch_vgg16'

    cfg.DNN['train'].MAX_SIZE = 224
    cfg.DNN['test'].MAX_SIZE = 224

    #cfg.DNN.PROPOSAL.TRAIN.SELECTION_BATCH_SIZE = {'s1':128,'s2':128}
    #cfg.DNN.FRCNN.TRAIN.SELECTION_BATCH_SIZE = {'s1':256,'s2':256,'s2_hinge':2000}

    cfg.NETWORK.INTERFACE = 'patch'
    cfg.NETWORK.FEAT_NAME = 'vgg16'

    return cfg
