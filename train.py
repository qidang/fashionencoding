import _init
import os
import argparse
import pickle
import platform
import progressbar
import numpy as np

#if __name__=="__main__" :
#    parser = argparse.ArgumentParser(description='Detector Training')
#    parser.add_argument('-c','--config',help='Configuration Name',required=True)
#    parser.add_argument('-b','--batchsize',help='Batch size', default=16 )
#    parser.add_argument('-t','--tag',help='Data Tag', default=None)
#    parser.add_argument('--datasetpath', help='Path to the image dataset', required=True )
#    parser.add_argument('--projectpath', help='Path to save the data', required=True )
#    parser.add_argument('-g','--gpu',help='GPU Index',default='0')
#    parser.set_defaults(finetune=False)
#    args = parser.parse_args()
#    #params = _init.parse_commandline()
#
#    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
#    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu


from PIL.ImageDraw import Draw
import tensorflow as tf

from data.datasets import load_dataset
from encoding_config import config
from tools import torch_tools
from data import data_feeder

from encoding_dnn.data import imageset_hdf5
from encoding_dnn.networks import networks

import torch
import torchvision.transforms as transforms

from dnn import trainer

def parse_commandline():
    parser = argparse.ArgumentParser(description="Training the Model")
    #parser.add_argument('-c','--config',help='Configuration Name', required=True)
    #parser.add_argument('--patchsize',help='Patch Size', required=True)
    parser.add_argument('-b','--batch_size',help='Batch size', required=True)
    parser.add_argument('-t','--tag',help='Model tag', required=True)
    #parser.add_argument('-g','--gpu',help='GPU Index', default='0')
    #parser.add_argument('--datasetpath',help='Path to the dataset',required=True)
    #parser.add_argument('--projectpath',help='Path to the project',required=True)
    return vars(parser.parse_args())

class my_trainer(trainer):
    def _train( self ):
        self._model['net'].train()

        widgets = [ progressbar.Percentage(), ' ', progressbar.ETA(), ' ',
                    '(',progressbar.DynamicMessage('loss'),')' ]
        bar = progressbar.ProgressBar(widgets=widgets,max_value=len(self._trainset)).start()

        loss_values = []

        for idx in range( len(self._trainset) ):
            inputs, targets = self._trainset.next()

            inputs = self._set_device( inputs )
            targets = self._set_device( targets )

            outputs = self._model['net']( inputs)
            loss = self._model['loss']( outputs, targets )

            loss_values.append( loss.cpu().detach().numpy() )

            # Computing gradient and do SGD step
            self._optimizer.zero_grad()
            loss.backward()
            self._optimizer.step()

            bar.update(idx+1,loss=loss.item())
        bar.finish()

        print('Average loss : ', np.mean(loss_values))

def get_dataset():
  #args = parse_commandline()
    params = parse_commandline()
    params['config'] = 'patch_dataset'
    params['gpu'] = '0'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    train_dataset = load_dataset( cfg, ['deepfashion','capb','train'] )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], train_dataset.hash, model_hash='vgg16' )

    train_imageset = imageset_hdf5(cfg, train_dataset, is_training=True, randomize=True)

    train_feeder = data_feeder(train_imageset)

    return train_feeder

def get_input(train_feeder):
    data = train_feeder.next()
    for key in data.keys():
        for a_key in data[key].keys():
            print('Key: {}, shape: {}'.format(a_key, data[key][a_key].shape))
    #return data

if __name__=="__main__" :
    params = parse_commandline()
    params['config'] = 'patch_dataset'
    params['gpu'] = '0'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    #trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    train_dataset = load_dataset( cfg, ['deepfashion','capb','train'] )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], train_dataset.hash, model_hash='vgg16' )

    train_imageset = imageset_hdf5(cfg, train_dataset, is_training=True, randomize=True)

    train_feeder = data_feeder(train_imageset)

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    model = {}
    #model['net'] = networks['gtbox'].Net( cfg ).to( device )
    #model['loss'] = networks['gtbox'].Loss( cfg ).to( device )
    model['net'] = networks.category_embedding.net(cfg).to(device)
    model['loss'] = networks.category_embedding.loss(cfg).to(device)

    t = my_trainer( cfg, model, device, train_feeder )
    #loss = t.trainstep()
    #print('loss is {}'.format(loss))
    t.train()

    train_feeder.exit()

    torch.save({'state_dict':model['net'].state_dict()}, cfg.path.MODEL)

    print('Model is saved to :', cfg.path.MODEL)
