#import sys
#sys.path.append('../')
import _init
import numpy as np
import h5py
import platform
import random
import itertools
import time
import os
import pickle
#from data.dataset import deepfashion_dataset
from data import workbench
from tools import torch_tools
from encoding_config import config
from encoding_dnn.data import patch_blobs

def get_cat_list(images):
    cat_num = []
    cat_list = [] # the number in cat_list is the index number of the image in the list
    cat_rev_list = []
    for i in range(50):
        #cat_num.append({'id':i+1, 'num':0})
        cat_list.append([])
        cat_rev_list.append([])
    for i, image in enumerate(images):
        #cat_num[int(image.category)]['num']+=1
        cat_list[int(image.category)].append(i)
        for j in range(50):
            if j!=i:
                cat_rev_list[j].append(i)
    return cat_list, cat_rev_list

def sample_pairs(a_list, number):
    combinations = list(itertools.combinations(a_list, 2))
    pairs = random.sample(combinations, number)
    return pairs

def select_triplets(images, max_pair_per_cat=5000):
    '''
    return the numpy array of triplets that need to be used, shape = pair_num*3 
    order: (anchor, positive, negtive)
    max_pair_per_cat means that you can have at most this number of positive pairs per class
    '''
    # image: {category : 50 kind, gtlabel: 3 type}
    cat_list, cat_rev_list = get_cat_list(images)
    
    triplets = None
    for i in range(50):
        print('process category {}'.format(i))
        a_list = cat_list[i]
        rev_list = cat_rev_list[i]
        list_len = len(a_list)
        if list_len <2:
            continue

        if list_len*(list_len-1)/2 <= max_pair_per_cat:
            pos_pair = list(itertools.combinations(a_list, 2))
        else:
            if list_len>20000:
                a_list = random.sample(a_list, 20000)
            pos_pair = sample_pairs(a_list, max_pair_per_cat)
        pos_pair_num = len(pos_pair)
        neg_sample = random.sample(rev_list, pos_pair_num)
        trip_tmp = np.column_stack((np.array(pos_pair), np.array(neg_sample)))
        if triplets is None:
            triplets = trip_tmp
        else:
            triplets = np.concatenate((triplets, trip_tmp), axis=0)

    np.random.shuffle(triplets)
    return triplets
    


def save_data( dataset, save_path, blobs_gen, patch_size ):
    images = dataset.images

    print('Selecting triplets...')
    start = time.time()

    # save catch 
    trip_file = 'cache_trip.pkl'
    if os.path.exists(trip_file):
        print('Load triplets selection from cache')
        with open(trip_file, 'rb') as f:
            triplets = pickle.load(f)
    else:
        triplets = select_triplets(images)
        with open(trip_file, 'wb') as f:
            pickle.dump(triplets, f)

    print('Triplets selection done. It takes {:.2f} seconds'.format(time.time()-start))
    ndata = len(triplets)

    with h5py.File(save_path, 'w') as h5 :
        anchor_data = h5.create_dataset("anchor_data", shape=(ndata, 3, patch_size, patch_size), dtype=np.float32)
        pos_data = h5.create_dataset("pos_data", shape=(ndata, 3, patch_size, patch_size), dtype=np.float32)
        neg_data = h5.create_dataset("neg_data", shape=(ndata, 3, patch_size, patch_size), dtype=np.float32)
        # labels = h5.create_dataset("data", shape=(ndata, 2), dtype=np.float32)

        for idx, triplet in enumerate( triplets ):
            #print( image.name )
            blobs, targets = blobs_gen.get_triplets_blobs(images, [triplet])
            #print('shape of anchor_data is {}'.format(blobs['inputs']['anchor_data'].size()))

            anchor_data[idx] = blobs['inputs']['anchor_data']
            pos_data[idx] = blobs['inputs']['pos_data']
            neg_data[idx] = blobs['inputs']['neg_data']

    print( save_path )

def get_dataset():
  #args = parse_commandline()

    params = {}
    params['config'] = 'patch_dataset'
    params['batch_size'] = 32
    params['gpu'] = '0'
    params['tag'] = '20181031'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'train']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], trainbench.dataset.hash, trainbench.model.hash )

    #load the dataset
    trainbench.load_dataset()
    #testbench.load_dataset([])

    train_set = trainbench.dataset
    #test_set = testbench.dataset
    return train_set

if __name__=="__main__" :
    params = {}
    params['config'] = 'patch_dataset'
    params['batch_size'] = 32
    params['gpu'] = '0'
    params['tag'] = '20181031'
    params['path'] = {}
    #path for save data
    
    if platform.system() == 'Linux':
        params['path']['project'] = '/home/qi/Vision/data'
    else:
        params['path']['project'] = '/Users/qida0163/Vision/data'
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )

    #get workbench
    trainbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )
    #testbench = workbench( cfg, {'dset_params':['deepfashion', 'capb', 'test']} )

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], trainbench.dataset.hash, trainbench.model.hash )

    #load the dataset
    trainbench.load_dataset()
    #testbench.load_dataset([])

    train_set = trainbench.dataset
    #test_set = testbench.dataset
  

    patch_size = 64
    train_path = cfg.path.HDF5 % ( train_set.hash )
    blobs_gen = patch_blobs(cfg.dnn)
    save_data(train_set, train_path, blobs_gen, patch_size)

    #blobs_gen = patch_blobs( cfg )
    #trainset = dataset( trainbench.dataset.images, blobs_gen,
    #                    batch_size=cfg.dnn.NETWORK.BATCH_SIZE,
    #                    randomize=True, training=True )

    #testset = dataset( testbench.dataset.images, blobs_gen,
    #                   batch_size=cfg.dnn.NETWORK.BATCH_SIZE,
    #                   training=False,
    #                   benchmark=networks.landmarks_patch.benchmark )
    ##testset = None

    #model = {}
    #model['net'] = networks.landmarks_patch.net( cfg ).to( device )
    #model['loss'] = networks.landmarks_patch.loss( cfg ).to( device )

    #criterias = ['accuracy', 'NE']
    #tt = trainer( cfg, model, device, trainset, testset, criterias )
    ##tt = trainer( cfg, model, device, trainset )
    ##tt.test()
    #tt.train()

    #torch.save( model['net'].state_dict(), cfg.path.MODEL )
    #print('Saved to %s' % (cfg.path.MODEL))
