import numpy as np
from tools import pil_tools
from PIL.ImageDraw import Draw
from dnn.blobs.blobs import data_blobs

import torch
import torchvision.transforms as transforms

class patch_blobs( data_blobs ):
    def __init__( self, dnn_cfg ):
        super().__init__( dnn_cfg )
        self._toTensor = transforms.ToTensor()
        self._normalizeTensor = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                     std=[0.229, 0.224, 0.225])

    def _add_data( self, images, blobs, training ):
        data = []

        im_size = self._dnn_cfg.MAX_SIZE

        for image in images :
            assert len( image.gtboxes ) == 1
            im = image.im_PIL.convert('RGB')

            box = image.gtboxes[0]
            box = np.floor( box ).astype( int )

            # Extracting the patch
            patch = im.crop((box[0],box[1],box[2],box[3]))
            patch = patch.resize((im_size, im_size))

            pt_patch = self._toTensor( patch )
            pt_patch = self._normalizeTensor( pt_patch )

            data.append( pt_patch )

        blobs['data'] = torch.stack( data )

    def _add_categories( self, images, blobs, training ):
        categories = []

        for image in images :
            c = int(image.category.ravel()[0])
            categories.append( c )

        gtcategory = np.array( categories, dtype=np.int64 )
        blobs['gtcategory'] = torch.from_numpy( gtcategory )

    def _get_patch(self, image):
        im_size = self._dnn_cfg.MAX_SIZE
        assert len( image.gtboxes ) == 1
        im = image.im_PIL.convert('RGB')

        box = image.gtboxes[0]
        box = np.floor( box ).astype( int )

        # Extracting the patch
        patch = im.crop((box[0],box[1],box[2],box[3]))
        patch = patch.resize((im_size, im_size))

        pt_patch = self._toTensor( patch )
        pt_patch = self._normalizeTensor( pt_patch )
        return pt_patch


    def _add_tri_data(self, images, blobs, tri_list):
        anchor_data = []
        pos_data = []
        neg_data = []

        for triplet in tri_list:
            anchor_patch = self._get_patch(images[triplet[0]])
            pos_patch = self._get_patch(images[triplet[1]])
            neg_patch = self._get_patch(images[triplet[2]])

            anchor_data.append(anchor_patch)
            pos_data.append(pos_patch)
            neg_data.append(neg_patch)

        blobs['anchor_data'] = torch.stack(anchor_data)
        blobs['pos_data'] = torch.stack(pos_data)
        blobs['neg_data'] = torch.stack(neg_data)


    def get_blobs( self, images, training ):
        blobs = {}

        # Converting the data to the propper format
        self._add_data( images, blobs, training )
        self._add_categories( images, blobs, training )

        inputs = {}
        inputs['data'] = blobs['data']

        targets = {}
        targets['gtcategory'] = blobs['gtcategory']

        return blobs, targets

    def get_triplets_blobs( self, images, triplet_list ):
        blobs = {}
        self._add_tri_data(images, blobs, triplet_list)

        inputs = {}
        inputs['anchor_data'] = blobs['anchor_data']
        inputs['pos_data'] = blobs['pos_data']
        inputs['neg_data'] = blobs['neg_data']

        blobs['inputs'] = inputs
        targets = {}
        return blobs, targets
