import numpy as np
import h5py
import torch
from data import imageset

class imageset_blob(imageset) :
    def __init__( self, cfg, dataset, blob_gen, randomize=False, is_training=False ):
        self._cfg = cfg
        self._dataset = dataset
        self._blob_gen = blob_gen
        self._images = dataset.images
        self._batchsize = cfg.dnn.NETWORK.BATCH_SIZE
        self._file = None
        self._randomize = randomize
        self._is_training = is_training

        self._prepare()

    def _prepare(self):
        ndata = len(self._images)
        inds = np.arange(ndata)
        batchsize = self._batchsize
        
        if self._randomize:
            inds = np.random.shuffle(inds)
        
        self._chunks = [inds[i:i+batchsize] for i in np.arange(0, ndata, batchsize)]
        self._cur = 0

    def _fix_batches( self, roibatches ):
        roibatches = roibatches.ravel()

        for i in range(len(roibatches)) :
            roibatches[i] = i

        return roibatches.reshape((-1,1)).astype(np.int32)

    def __len__( self ):
        return len(self._chunks)

    def __getitem__( self, idx ):
        images = self._images[self._chunks[idx]]
        blobs = self._blob_gen.get_blobs(images, training=self._is_training)

        return blobs

    def next( self ):
        if self._cur >= len(self._chunks):
            self._cur = 0
        blobs = self[self._cur]
        self._cur = self._cur+1
        return blobs
