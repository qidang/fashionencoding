from easydict import EasyDict as edict
from . import category_patch
from . import category_embedding
#from . import category_gtbox

networks = edict()
networks.category_patch = edict()
networks.category_patch.net = category_patch.Net
networks.category_patch.loss = category_patch.Loss
networks.category_patch.benchmark = category_patch.Benchmark

networks.category_embedding = edict()
networks.category_embedding.net = category_embedding.Net
networks.category_embedding.testnet = category_embedding.EmbeddingNet
networks.category_embedding.loss = category_embedding.Loss
networks.category_embedding.benchmark = category_embedding.Benchmark
#networks.category_gtbox = edict()
#networks.category_gtbox.net = category_gtbox.Net
#networks.category_gtbox.loss = category_gtbox.Loss
#networks.category_gtbox.benchmark = category_gtbox.Benchmark
