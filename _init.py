import sys
import os

def add_path( p ):
    if p not in sys.path :
        sys.path.append(p)

add_path('./torchcore/')